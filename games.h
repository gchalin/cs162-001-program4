/*
	Hayden Chalin
	CS 162
	Program 4 
	03/07/2024
	The purpose of this program is to allow the user to temporaly store thier games.
	The program will first ask the user how many games they would like to store, then
	dynamically allocate the memory needed.
*/
#include <iostream>
#include <cctype>
#include <cstring>

using namespace std;

/*
	This file should include:
		1: #includes
		2: structs
		3: prototypes
		4: class interfaces
		5: constants
*/

/*
	MENU CHOICES
*/
const int ADD_GAME{1};
const int DISPLAY_ALL {2};
const int DISPLAY_GENRE {3};
const int SEARCH {4}; // By name
const int QUIT {5};

/*
	PROTOTYPES
*/
void display_Menu(int & menu_Choice);
int get_Size();

// Game class
class Game{
	public:
		Game(); // constructor
		~Game(); // destructor
		void display(int game_Num); // used to display a game along with its position in the array
		void add();
		void update(); // This will update all properties of the instance
		bool is_Game(); // Used to determin if it is a filled out game vs a empty game (needs to be filled out)
		bool genre_Match(char user_Input[]); // Returns true or false if the searched genre matches
		bool name_Match(char user_Input[]); // Returns true of false if the searched name matches

	private:
		char *name;
		char *review;
		char *genre;
		int *num_Players;
		int *playable_Hours;
		bool *is_Multiplayer;

};

// Game list class
class Game_List{
	
	public:
		Game_List(int size); // Constructor
		~Game_List(); // Destructor
		void add_Game(); // adds a game to the list	
		void display_All(); // displays all games in list
		void display_By_Genre(); // displays all games matching a genre
		void display_By_Name(); // find a game by name

				

	private: // Data members
		int count;
		int arr_Size; // this will be the size of the array dynamically allocated (obtained from user)
		Game *list; // pointer to a list 
		bool is_Empty; 

};

