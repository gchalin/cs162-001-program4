/*
	Hayden Chalin
	CS 162
	Program 4 
	03/07/2024
	The purpose of this program is to allow the user to temporaly store thier games.
	The program will first ask the user how many games they would like to store, then
	dynamically allocate the memory needed.
*/
#include "games.h"


/*
	Game list
*/
Game_List::Game_List(int size){
	/*
		This is the constructor of the Game list class
	@param size: represents the array size of the memory to be allocated
	*/
	count = 0;
	arr_Size = size; // update array size with size obtained from user
	list = new Game[size]; // Pointer to the first game
	is_Empty = true;
		
}

Game_List::~Game_List(){
	/*
		This will loop through the game list and dealocate all related memory
	*/
	delete [] list;// dealocate the memory

}

void Game_List::add_Game(){
	/*
		This method will add a game to the game list by calling the method on the game class.
	*/
	cout << "Add new" << endl;
	cout << "Count: " << count << endl;
	cout << "Size: " << arr_Size << endl;
	if ( arr_Size - count){ // 0 = false
		list[count].update();
		++count;
		is_Empty = false;
	} else {
		cout << "You cannot add any more games" << endl;
	}
}

void Game_List::display_All(){
/*
	This method will print out all of the games in the array
*/
	cout << "Display all" << endl;
	if (is_Empty){ // if there is nothing in the first index
		cout << "there are no games to display" << endl;
		cout << endl;
	} else {
		for (int i {0}; i < count; ++i){ // while the name has a length (t/f)
				list[i].display(i+1);
		}	
	}	
}

void Game_List::display_By_Genre(){
	/*
		This function will prompt the user to search for a genre
	*/  	
	char user_Input[51];
	bool matched = false;
	cout << "What genre do you want to display: ";
	cin.get(user_Input, 51, '\n');
	cin.ignore(100, '\n');
	
	// loop through the list
	for (int i {0}; i < count; ++i){
		if (list[i].genre_Match(user_Input)){ // check instance for genre match
			list[i].display(i+1);
			matched = true;
		}
	}

	if (!matched) // If there is no matches display a message
		cout << "No matches were found, please try again!" << endl;
		
}

void Game_List::display_By_Name(){
	/*
		This function will prompt the user to search for a name
	*/  	
	char user_Input[51];
	bool matched = false;
	cout << "What game do you want to search for?";
	cin.get(user_Input, 51, '\n');
	cin.ignore(100, '\n');
	
	// loop through the list
	for (int i {0}; i < count; ++i){
		if (list[i].name_Match(user_Input)){ // check instance for genre match
			list[i].display(i+1);
			matched = true; // used for ui below
		}
	}

	if (!matched) // If there is no matches display a message
		cout << "No matches were found, please try again!" << endl;
	

}

/*
 Game class constructor
*/

Game::Game(){
	/*
		This is the game constructor called when the user dynamically allocates the memory of the 
		games_list array.
	*/
	name = nullptr; // initialize to nullptr
	review = nullptr;
	genre = nullptr;
	is_Multiplayer = new bool;
	num_Players = new int; // initialize the pointer to a new integer
	playable_Hours = new int;
}

Game::~Game(){
	/* 
		 This will be called for each instance of a game in the list.  This will deallocate 
		 memory after the application is done.
	*/
	delete [] name;
	delete [] review;
	delete [] genre;
	delete num_Players;	
	delete playable_Hours;
	delete is_Multiplayer;
}

void Game::display(int game_Num){
	/*
		This function displays a game and uses the game_Num to indicate its position in the array
	*/
	if (is_Game()){
		cout << "Game " << game_Num << ":" << endl;
		cout << "\tName: " <<  name << endl;
		cout << "\tReview: " <<  review << endl;
		cout << "\tGenre: " <<  genre << endl;
		cout << "\tNumber of players: " << *num_Players << endl; // the '*' is a dereference op in this case
		cout << "\tMultiplayer: " << (*is_Multiplayer ? "Yes" : "No" ) << endl;	
	}
}

void Game::update(){
	/*
		This function will update the intance of the game
	*/
	// Variables
	char temp_Name[21];
	char temp_Review[300];
	char temp_Genre[51];
	char user_Input; //Used for multiplayer bool
	
	// Update name
	cout << "What is the name of the game: ";
	cin.get(temp_Name, 21, '\n');  
	cin.ignore(100,'\n');
	name = new char[strlen(temp_Name) + 1];
	strcpy(name, temp_Name);	

	// Reveiw
	cout << "Review: ";
	cin.get(temp_Review, 300, '\n');
	cin.ignore(100, '\n');
	review = new char[strlen(temp_Review) + 1];
	strcpy(review, temp_Review);

	// Genre
	cout << "Genre: ";
	cin.get(temp_Genre, 51, '\n');
	cin.ignore(100, '\n');
	genre = new char[strlen(temp_Genre) + 1];
	strcpy(genre, temp_Genre);

	// Num Players
	cout << "Number of Players: ";
	cin >> *num_Players;
	cin.ignore(100, '\n');
		
	// Multiplayer
	cout << "Is the game multiplayer(y/n):";
	cin >> user_Input;
	cin.ignore(100, '\n');
	*is_Multiplayer = (user_Input == 'y' || user_Input == 'n');
	

}

bool Game::is_Game(){
	/*
		This method will determin if this game instance is empty or not
	*/
	bool res = false;
	if (strlen(name))
		res =  true; // update res if it exists
	return res;
}

bool Game::genre_Match(char user_Input[51]){
	/*
		This will determine if a games genre matches the users input in list and return a bool
	*/
	if ( strcmp(genre, user_Input) == 0 ){
		return true;
	} else {
		return false;
	}

}

bool Game::name_Match(char user_Input[51]){
	/*
		This will determine if a games name matches the users input in list and return a bool
	*/
	if ( strcmp(name, user_Input) == 0 ){
		return true;
	} else {
		return false;
	}

}
