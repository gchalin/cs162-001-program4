
/*
	Hayden Chalin
	CS 162
	Program 4 
	03/07/2024
	The purpose of this program is to allow the user to temporaly store thier games.
	The program will first ask the user how many games they would like to store, then
	dynamically allocate the memory needed.
*/
#include "games.cpp"
int main(){
	
	int menu_Choice {0};
	int size = get_Size(); // array size for dynamic memory 

	// initialize a game list
	Game_List game_List(size);

	//Menu loop
	while (menu_Choice != QUIT){	
		// Display the menu
		display_Menu(menu_Choice);	
		// Menu choice filter
		switch (menu_Choice){
			case ADD_GAME:
				game_List.add_Game();
				break;
			case DISPLAY_ALL: 
				game_List.display_All();
				break;
			case DISPLAY_GENRE:	 
				game_List.display_By_Genre();
				break;
			case SEARCH:
				game_List.display_By_Name();
				break;
			case QUIT:
				cout << "You have quit the program" << endl;
				break;
		}
	}
	return 0;
}

void display_Menu(int & menu_Choice){
	/*
		This function displays the menu and updates the menuChoice
	*/
	cout << endl;
	cout << "*******************************************************" << endl;	
	cout << "This is the menu, please enter a menu choice!" << endl;
	cout << "1) Add new game" << endl;
	cout << "2) Display all games" << endl;
	cout << "3) Display by genre" << endl;
	cout << "4) Search for a game by name" << endl;
	cout << "5) Quit the program" << endl;
	cout << "Choice: ";
	cin >> menu_Choice;
	cin.ignore(100, '\n'); 

}

int get_Size(){
	/*
		This function returns an int that represents the size of the array to be 
		dynamically allocated.

	*/
	int size; 
	cout << "How many games are you expecting to add? (This cant be changed later)";
	cin >> size;
	cin.ignore(100, '\n');
	cout << "An array has be allocated of size: " << size << endl;		
	return size;	

}
